
import java.util.Scanner;

public class Game {
	
	static char[][] oxtable = new char[3][3];
	static char turn = 'O';

	public static void showWelcome() {
		System.out.println("  Welcome to OXgame V.3");
	}
	
	public static void showTurn(char turn) {
		System.out.println("Turn "+turn);
	}
	
	public static void showTable(char oxtable[][]){
		System.out.println("==========================");
        for(int i = 0 ;i < 3;i ++){
	            for(int j = 0 ;j < 3;j ++){
	            	if(oxtable[i][j]==0) {
	            		System.out.print("-");
	            	}else {
	            		System.out.print(oxtable[i][j]);
	            	}
	               System.out.print(" ");  
	        }
	            System.out.println("");
	        }
	}
	
	static void input(char oxtable[][],char turn){
		showTurn(turn);
    	System.out.print("Please input row, col: ");
        Scanner input = new Scanner(System.in);
        int row = input.nextInt()-1;
        int col = input.nextInt()-1;
        checkInput(row,col,oxtable,turn);
	}
	
	static void addInput(int row,int col,char oxtable[][],char turn){
		oxtable[row][col] = turn;
		checkWin(row,col,oxtable,turn);
		checkDraw(oxtable);
	}
	
	static void checkInput(int row,int col,char oxtable[][],char turn){
		if(row < 3 && row > -1 && col < 3 && col > -1) {
			if(oxtable[row][col]== 0) { 
				addInput(row,col,oxtable,turn);
            }else {
        	System.out.println("NO!! row, col is not empty.");
        	showTable(oxtable);
        	input(oxtable,turn);
            }
		}else {
            System.out.println("NO!! is not row, col in game.");
            showTable(oxtable);
            input(oxtable,turn);
		}
	}

	static char swifTurn(char turn){
    	if(turn=='O') {
    		return 'X';
    	}else {
    		return 'O';
    	}
    }
	
	static void checkWin(int row,int col,char oxtable[][],char turn){
		checkWinRow(row,oxtable,turn);
		checkWinCol(col,oxtable,turn);
		checkWinCross1(oxtable,turn);
		checkWinCross2(oxtable,turn);
	}
	
	static void showWin(char oxtable[][],char turn){
		showTable(oxtable);
		System.out.println("==========================");
		System.out.println(turn+" WIN !!");
	    System.exit(1);  
	}
	
	static void checkWinRow(int row,char oxtable[][],char turn) {
		if( oxtable[row][0] == turn
			&& oxtable[row][1] == turn	
			&& oxtable[row][2] == turn	) {
			showWin(oxtable,turn);
		}

	}
	
	static void checkWinCol(int Col,char oxtable[][],char turn) {
		if( oxtable[0][Col] == turn
			&& oxtable[1][Col] == turn	
			&& oxtable[2][Col] == turn	) {
			showWin(oxtable,turn);
		}

	}
	
	static void checkWinCross1(char oxtable[][],char turn){   	
		if( oxtable[0][0] == turn
				&& oxtable[1][1] == turn	
				&& oxtable[2][2] == turn	) {
			showWin(oxtable,turn);
			} 		
    }
	
	static void checkWinCross2(char oxtable[][],char turn){   	
		if( oxtable[0][2] == turn
				&& oxtable[1][1] == turn	
				&& oxtable[2][0] == turn	) {
			showWin(oxtable,turn);
			} 		
    }
	
	static void checkDraw(char oxtable[][]){
        int draw = 0;
        for(int i = 0 ;i < 3;i ++){
            for(int j = 0 ;j < 3;j ++){
		if(oxtable[i][j]==0) {              
		}else {
                   draw = draw+1; 
                }
            }
	}
        if(draw == 9){
        	showTable(oxtable);
        	System.out.println("==========================");
            System.out.println("DRAW!!");
            System.exit(1); 
        } 
    }
	
	public static void main(String[] triwit){
			showWelcome();
			for(;;) {
			showTable(oxtable);
			input(oxtable,turn);
			turn = swifTurn(turn);
			}
			
			
			
	}
}
